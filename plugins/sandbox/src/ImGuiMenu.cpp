#include "ImGuiMenu.h"
#include <Events.h>
#include <Windows.h>
#include <detours.h>
#include <imgui.h>
#include <imgui_impl_dx9.h>
#include <imgui_impl_win32.h>
#include <d3dx9.h>
#include "Direct3D9Proxy.h"
#include <game_sa/RenderWare.h>

// 0x745560 ; int __cdecl InitInstance(HINSTANCE hInstance)
static auto InitInstance = reinterpret_cast<HWND(__cdecl *)(HINSTANCE)>(0x745560);

// 0x53E230 ; void __cdecl Render2dStuff()
static auto Render2dStuff = reinterpret_cast<void (__cdecl *)()>(0x53E230);

using Direct3DCreate9_t = IDirect3D9 * (WINAPI *)(unsigned int);
static Direct3DCreate9_t Direct3DCreate9_Game = nullptr;

// 0x53D7A0 ; bool __cdecl DoRWStuffStartOfFrame_Horizon()
static auto DoRWStuffStartOfFrame_Horizon = reinterpret_cast<bool (__cdecl *)()>(0x53D7A0);

// 0x734640 ; void __cdecl FlushObrsPrintfs()
static auto FlushObrsPrintfs = reinterpret_cast<void(__cdecl *)()>(0x734640);

// 0x8D621C ; BOOL ForegroundApp
static auto& ForegroundApp = *reinterpret_cast<BOOL *>(0x8D621C);

IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

static WNDPROC GameProc = nullptr;
static bool showingDemoWindow = false;

LRESULT CALLBACK SandboxWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{ 
    if (ImGui_ImplWin32_WndProcHandler(hWnd, Msg, wParam, lParam))
        return 0;

    if (!showingDemoWindow) {
        if (Msg == WM_KEYDOWN && wParam == VK_ESCAPE) {
            GameProc(hWnd, WM_ACTIVATE, WA_INACTIVE, reinterpret_cast<LPARAM>(hWnd));
            showingDemoWindow = true;
            ClipCursor(NULL);
            while (ShowCursor(TRUE) < 0);
            return 0;
        }
    }
    else {
        if (Msg == WM_KEYDOWN && wParam == VK_ESCAPE) {
            showingDemoWindow = false;
            while (ShowCursor(FALSE) >= 0);
            GameProc(hWnd, WM_ACTIVATE, WA_ACTIVE, reinterpret_cast<LPARAM>(hWnd));
            return 0;
        }

        if (Msg == WM_ACTIVATE)
            return 0;
    }

    if (ImGui::GetCurrentContext()) {
        ImGuiIO& io = ImGui::GetIO();

        if (io.WantCaptureKeyboard)
            return 0;
    }

    return GameProc(hWnd, Msg, wParam, lParam);
}

static HWND __cdecl InitInstance_Hook(HINSTANCE hInstance)
{
    HWND const hWnd = InitInstance(hInstance);
    LONG_PTR pGameProc = SetWindowLongPtrA(hWnd, GWL_WNDPROC, reinterpret_cast<LONG_PTR>(&SandboxWndProc));
    GameProc = reinterpret_cast<WNDPROC>(pGameProc);
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui_ImplWin32_Init(hWnd);
    ImGui::StyleColorsDark();
    ImGuiIO& io = ImGui::GetIO();
    io.Fonts->AddFontDefault();
    return hWnd;
}

static void __cdecl Render2dStuff_Hook()
{
    Render2dStuff();

    if (showingDemoWindow) {
        ImGui::ShowDemoWindow(&showingDemoWindow);

        if (!showingDemoWindow) {
            while (ShowCursor(FALSE) >= 0);
            HWND const hWnd = RsGlobal.ps->window;
            GameProc(hWnd, WM_ACTIVATE, WA_ACTIVE, reinterpret_cast<LPARAM>(hWnd));
        }
    }
}

static IDirect3D9 * WINAPI Direct3DCreate9_Hook(unsigned int SDKVersion)
{
    IDirect3D9 * const pDirect3D9 = Direct3DCreate9_Game(SDKVersion);

    if (!pDirect3D9)
        return nullptr;

    return new Direct3D9Proxy(pDirect3D9);
}

static bool __cdecl DoRWStuffStartOfFrame_Horizon_Hook()
{
    if (!DoRWStuffStartOfFrame_Horizon())
        return false;

    ImGui_ImplDX9_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();
    return true;
}

static void __cdecl FlushObrsPrintfs_Hook()
{
    ImGui::EndFrame();
    ImGui::Render();
    ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
}

template <typename Variable, typename Hook>
static void Detour(Variable v, Hook h)
{
    auto ppPointer = reinterpret_cast<PVOID *>(v);
    auto pDetour   = reinterpret_cast<PVOID>(h);
    DetourAttach(ppPointer, pDetour);
}

static void DetourFunctions()
{
    Direct3DCreate9_Game = reinterpret_cast<Direct3DCreate9_t>(DetourFindFunction("d3d9.dll", "Direct3DCreate9"));

    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    Detour(&InitInstance, &InitInstance_Hook);
    Detour(&Render2dStuff, &Render2dStuff_Hook);
    Detour(&Direct3DCreate9_Game, &Direct3DCreate9_Hook);
    Detour(&DoRWStuffStartOfFrame_Horizon, &DoRWStuffStartOfFrame_Horizon_Hook);
    Detour(&FlushObrsPrintfs, &FlushObrsPrintfs_Hook);
    DetourTransactionCommit();
}

void EnableImGui()
{
    DetourFunctions();
}

void DisableImGui()
{
    ImGui_ImplDX9_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();
}
