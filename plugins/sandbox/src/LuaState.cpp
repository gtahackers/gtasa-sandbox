#include "LuaState.h"
#include <iostream>
#include <lua.hpp>

int CreateLuaState(void)
{
    lua_State * const L = luaL_newstate();

    if (L == nullptr) {
        std::cerr << "Cannot create a new Lua state: not enough memory\n";
        return EXIT_FAILURE;
    }

    luaL_openlibs(L);

    int result = luaL_loadstring(L, R"(
        print("Hello World (Lua edition)\n")
    )");

    if (result != LUA_OK) {
        std::cerr << "Cannot load string into Lua state: ";

        switch (result) {
            case LUA_ERRSYNTAX:
                std::cerr << "syntax error during precompilation\n";
                break;
            case LUA_ERRMEM:
                std::cerr << "memory allocation (out-of-memory) error\n";
                break;
            case LUA_ERRGCMM:
                std::cerr << "error while running a __gc metamethod\n";
                break;
            default:
                std::cerr << "unknown error\n";
                break;
        }

        lua_close(L);
        return EXIT_FAILURE;
    }

    result = lua_pcall(L, 0, 0, 0);

    if (result != LUA_OK) {
        std::cerr << "Failed to run loaded chunk: ";

        switch (result) {
            case LUA_ERRRUN:
                std::cerr << "runtime error\n";
                break;
            case LUA_ERRMEM:
                std::cerr << "memory allocation error\n";
                break;
            case LUA_ERRERR:
                std::cerr << "error while running the message handler\n";
                break;
            case LUA_ERRGCMM:
                std::cerr << "error while running a __gc metamethod\n";
                break;
            default:
                std::cerr << "unknown error\n";
                break;
        }

        lua_close(L);
        return EXIT_FAILURE;
    }

    lua_close(L);
    return EXIT_SUCCESS;
}
