#include "psSelectDevice.h"
#include "../missing-in-plugin-sdk/Variables.h"
#include <algorithm>
#include <Windows.h>
#include <detours.h>
#include <game_sa/CMenuManager.h>
#include <game_sa/RenderWare.h>

#ifndef MAX_SUBSYSTEMS
#   define MAX_SUBSYSTEMS (16)
#endif

// 0x746190 ; RwBool __cdecl psSelectDevice(RwBool)
static std::uintptr_t functionAddress = 0x746190;

static RwBool Function(RwBool)
{
    RwInt32 const numSubSystems = std::min<RwInt32>(RwEngineGetNumSubSystems(), MAX_SUBSYSTEMS);

    if (!numSubSystems)
        return FALSE;

    // RwSubSystemInfo subSystemInfo[MAX_SUBSYSTEMS] = { 0 };
    auto subSystemInfo = reinterpret_cast<RwSubSystemInfo *>(0xC8CFC0);

    for (RwInt32 i = 0; i < numSubSystems; ++i)
        RwEngineGetSubSystemInfo(&subSystemInfo[i], i);

    RwInt32 const subSystem = RwEngineGetCurrentSubSystem();
    RwInt32 const videoMode = RwEngineGetCurrentVideoMode();

    /*if (!RwEngineSetSubSystem(subSystem))
    return FALSE;

    if (!RwEngineSetVideoMode(videoMode))
    return FALSE;*/

    curSubSystem = subSystem;
    curVideoMode = videoMode;

    RwVideoMode vm = { 0 };
    RwEngineGetVideoModeInfo(&vm, videoMode);
    HWND const hWnd = RsGlobal.ps->window;

    // Hack: Force windowed mode
    vm.flags = RwVideoModeFlag(0);

    if (vm.flags & rwVIDEOMODEEXCLUSIVE) {
        RsGlobal.maximumWidth = vm.width;
        RsGlobal.maximumHeight = vm.height;
        RsGlobal.ps->fullScreen = TRUE;
        SetWindowLong(hWnd, GWL_STYLE, WS_POPUP);
        SetWindowPos(hWnd, 0, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
        RwD3D9EngineSetRefreshRate(vm.refRate);
    }
    else {
        RECT rect = { 0 };
        GetClientRect(hWnd, &rect);
        RsGlobal.maximumWidth = rect.right;
        RsGlobal.maximumHeight = rect.bottom;
        int x = (vm.width - rect.right) / 2;
        int y = (vm.height - rect.bottom) / 2;
        SetWindowPos(hWnd, 0, x, y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
    }

    RwD3D9EngineSetMultiSamplingLevels(*reinterpret_cast<RwUInt32 *>(0xBA6810));
    return TRUE;
}

bool psSelectDevice::Attach()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&functionAddress);
    auto pDetour   = reinterpret_cast<PVOID>(&Function);
    DetourAttach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}

bool psSelectDevice::Detach()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&functionAddress);
    auto pDetour   = reinterpret_cast<PVOID>(&Function);
    DetourDetach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}
