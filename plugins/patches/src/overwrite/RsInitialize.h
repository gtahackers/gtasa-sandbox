#pragma once

namespace RsInitialize
{
    bool Attach();
    bool Detach();
};
