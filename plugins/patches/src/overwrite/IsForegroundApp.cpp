#include "IsForegroundApp.h"
#include "../Globals.h"
#include "../missing-in-plugin-sdk/Variables.h"
#include <cstdint>
#include <Windows.h>
#include <detours.h>

// 0x746060 ; bool __cdecl IsForegroundApp()
static std::uintptr_t functionAddress = 0x746060;

static bool __cdecl Function()
{
    return ForegroundApp == TRUE && !ForegroundMenu;
}

bool IsForegroundApp::Attach()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<void **>(&functionAddress);
    auto pDetour   = reinterpret_cast<void *>(&Function);
    DetourAttach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}

bool IsForegroundApp::Detach()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<void **>(&functionAddress);
    auto pDetour   = reinterpret_cast<void *>(&Function);
    DetourDetach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}
