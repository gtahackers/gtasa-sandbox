#pragma once

namespace IsForegroundApp
{
    bool Attach();
    bool Detach();
};
