#include "MainWndProc.h"
#include "../Globals.h"
#include "../missing-in-plugin-sdk/Variables.h"
#include "../missing-in-plugin-sdk/CPostEffects.h"
#include "../missing-in-plugin-sdk/RenderWare.h"
#include <Windows.h>
#include <Windowsx.h>
#include <detours.h>
#include <cstdint>
#include <game_sa/CPad.h>
#include <game_sa/RenderWare.h>
#include <game_sa/CMenuManager.h>
#include <d3d9.h>

// Globals.h
bool ForegroundMenu = false;

// 0x747EB0 ; LRESULT CALLBACK MainWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
static std::uintptr_t functionAddress = 0x747EB0;

static LRESULT CALLBACK Function(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    static bool wasLastCursorPosSet = false;
    static POINT lastCursorPos = { 0 };

    switch (Msg) {
        case WM_CREATE:
        {
            ShowCursor(FALSE);
            return 0;
        }
        case WM_SIZE:
        {
            RwRect r = { 0 };
            r.w = LOWORD(lParam);
            r.h = HIWORD(lParam);

            if (RwInitialized && r.h > 0 && r.w > 0)
            {
                RsEventHandler(rsCAMERASIZE, reinterpret_cast<std::intptr_t>(&r));
                CPostEffects::SetupBackBufferVertex();

                if (r.w != LOWORD(lParam) && r.h != HIWORD(lParam))
                {
                    ReleaseCapture();

                    WINDOWPLACEMENT wp;
                    GetWindowPlacement(hWnd, &wp);

                    if (wp.showCmd == SW_SHOWMAXIMIZED)
                        SendMessage(hWnd, WM_WINDOWPOSCHANGED, 0, 0);
                }
            }

            return 0;
        }
        case WM_SIZING:
        {
            if (RwInitialized && gGameState == 9) {
                RsEventHandler(26, 1);
                CPostEffects::SetupBackBufferVertex();
            }

            return 0;
        }
        case WM_LBUTTONDOWN:
        {
            if (!ForegroundApp || ForegroundMenu)
                return 0;
            
            RsMouseStatus ms;
            POINTS points = MAKEPOINTS(lParam);
            ms.pos.x = points.x;
            ms.pos.y = points.y;
            ms.shift = (wParam & MK_SHIFT) ? TRUE : FALSE;
            ms.control = (wParam & MK_CONTROL) ? TRUE : FALSE;

            SetCapture(hWnd);
            RsMouseEventHandler(rsLEFTBUTTONDOWN, (void *)&ms);
            return 0;
        }
        case WM_RBUTTONDOWN:
        {
            if (!ForegroundApp || ForegroundMenu)
                return 0;
            
            RsMouseStatus ms;
            POINTS points = MAKEPOINTS(lParam);
            ms.pos.x = points.x;
            ms.pos.y = points.y;
            ms.shift = (wParam & MK_SHIFT) ? TRUE : FALSE;
            ms.control = (wParam & MK_CONTROL) ? TRUE : FALSE;

            SetCapture(hWnd);
            RsMouseEventHandler(rsRIGHTBUTTONDOWN, (void *)&ms);
            return 0;
        }
        case WM_MOUSEWHEEL:
        {
            if (!ForegroundApp || ForegroundMenu)
                return 0;
            
            RwBool forward = ((short)HIWORD(wParam) < 0) ? FALSE : TRUE;
            RsMouseEventHandler(rsMOUSEWHEELMOVE, (void *)&forward);
            return 0;
        }
        case WM_MOUSEMOVE:
        {
            if (!ForegroundApp || ForegroundMenu)
                return 0;
            
            // Keep mouse inside the window boundaries
            RECT rect = { 0 };
            GetWindowRect(hWnd, &rect);
            ClipCursor(&rect);

            static bool validDelta = false;
            POINTS points = MAKEPOINTS(lParam);

            if (validDelta) {
                RsMouseStatus ms;
                ms.delta.x = points.x - RsGlobal.ps->lastMousePos.x;
                ms.delta.y = points.y - RsGlobal.ps->lastMousePos.y;
                ms.pos.x = points.x;
                ms.pos.y = points.y;
                RsMouseEventHandler(rsMOUSEMOVE, &ms);
            }
            else 
                validDelta = true;

            RsGlobal.ps->lastMousePos.x = points.x;
            RsGlobal.ps->lastMousePos.y = points.y;
            FrontEndMenuManager.mousePosLeftA = GET_X_LPARAM(lParam);
            FrontEndMenuManager.mousePosTopA = GET_Y_LPARAM(lParam);
            return 0;
        }
        case WM_LBUTTONUP:
        {
            if (!ForegroundApp || ForegroundMenu)
                return 0;

            ReleaseCapture();
            RsMouseEventHandler(rsLEFTBUTTONUP, NULL);
            return 0;
        }
        case WM_RBUTTONUP:
        {
            if (!ForegroundApp || ForegroundMenu)
                return 0;
            
            ReleaseCapture();
            RsMouseEventHandler(rsRIGHTBUTTONUP, NULL);
            return 0;
        }
        case WM_KEYDOWN:
        case WM_SYSKEYDOWN:
        {
            if (wParam == VK_ESCAPE) {
                if (!ForegroundMenu) {
                    if (!wasLastCursorPosSet) {
                        RECT clientRect;
                        GetClientRect(hWnd, &clientRect);
                        lastCursorPos.x = (clientRect.right - clientRect.left) / 2;
                        lastCursorPos.y = (clientRect.bottom - clientRect.top) / 2;
                        wasLastCursorPosSet = true;
                    }

                    ClipCursor(NULL);
                    while (ShowCursor(TRUE) < 0);
                    ClientToScreen(hWnd, &lastCursorPos);
                    SetCursorPos(lastCursorPos.x, lastCursorPos.y);
                    CPad::GetPad(0)->Clear(false, true);
                    CPad::GetPad(1)->Clear(false, true);
                    CPad::GetPad(0)->bDisablePlayerJump = true;
                    ForegroundMenu = true;
                }
                else {
                    GetCursorPos(&lastCursorPos);
                    ScreenToClient(hWnd, &lastCursorPos);
                    while (ShowCursor(FALSE) >= 0);
                    CPad::GetPad(0)->Clear(true, true);
                    CPad::GetPad(1)->Clear(true, true);
                    CPad::GetPad(0)->bDisablePlayerJump = false;
                    ForegroundMenu = false;
                }
                
                return 0;
            }

            if (!ForegroundApp || ForegroundMenu)
                return 0;

            if (PopulateRwKeyCode(&lParam, lParam, wParam))
                RsKeyboardEventHandler(28, &lParam);
            
            if (wParam != 16)
                return 0;
            
            HandleAnySpecialVirtualKeyChecks(&lParam);
            return 0;
        }
        case WM_KEYUP:
        case WM_SYSKEYUP:
        {
            if (!ForegroundApp || ForegroundMenu)
                return 0;
            
            if (PopulateRwKeyCode(&lParam, lParam, wParam))
                RsKeyboardEventHandler(29, &lParam);

            if (wParam == 16)
                HandleAnySpecialVirtualKeyChecks(&lParam);
            
            return 0;
        }
        case WM_ACTIVATE:
        {
            if (LOWORD(wParam) == WA_INACTIVE) {
                SetTimer(hWnd, 1, 20, NULL);
                RsEventHandler(38, FALSE);
                ForegroundApp = FALSE;

                if (!ForegroundMenu)
                    ShowCursor(TRUE);
            }
            else {
                KillTimer(hWnd, 1);
                RsEventHandler(38, TRUE);
                ForegroundApp = TRUE;

                if (!ForegroundMenu)
                    ShowCursor(FALSE);
            }

            if (!ForegroundMenu) {
                CPad::GetPad(0)->Clear(false, true);
                CPad::GetPad(1)->Clear(false, true);
            }

            return 0;
        }
        case WM_ENTERSIZEMOVE:
        {
            SetTimer(hWnd, 1, 20, NULL);
            return 0;
        }
        case WM_EXITSIZEMOVE:
        {
            KillTimer(hWnd, 1);
            return 0;
        }
        case WM_MOVING:
        case WM_TIMER:
        {
            if (RwInitialized && gGameState == 9)
                RsEventHandler(26, 1);
            return 0;
        }
        case WM_CLOSE:
        case WM_DESTROY:
        {
            ClipCursor(NULL);
            FreeAllDirectInputObjects();
            PostQuitMessage(0);
            return 0;
        }
        default:
            break;
    }

    return DefWindowProcA(hWnd, Msg, wParam, lParam);
}

bool MainWndProc::Attach()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&functionAddress);
    auto pDetour   = reinterpret_cast<PVOID>(&Function);
    DetourAttach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}

bool MainWndProc::Detach()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&functionAddress);
    auto pDetour   = reinterpret_cast<PVOID>(&Function);
    DetourDetach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}
