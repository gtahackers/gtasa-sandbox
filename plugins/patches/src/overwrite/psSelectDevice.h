#pragma once

namespace psSelectDevice
{
    bool Attach();
    bool Detach();
};
