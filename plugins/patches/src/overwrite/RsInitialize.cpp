#include "RsInitialize.h"
#include "../missing-in-plugin-sdk/RenderWare.h"
#include <Windows.h>
#include <detours.h>
#include <cstdint>
#include <game_sa/RenderWare.h>

// 0x619600 ; RwInt32 __cdecl RsInitialize()
static std::uintptr_t functionAddress = 0x619600;

static RwInt32 Function()
{
    RsGlobal.appName = "GTA:SA Sandbox";
    RsGlobal.maximumWidth = 1024;
    RsGlobal.maximumHeight = 768;
    RsGlobal.frameLimit = 30;
    RsGlobal.quit = false;

    RsInputDevice &keyboard = RsGlobal.keyboard;
    keyboard.inputDeviceType = rsKEYBOARD;
    keyboard.inputEventHandler = nullptr;
    keyboard.used = false;

    RsInputDevice &mouse = RsGlobal.mouse;
    mouse.inputDeviceType = rsMOUSE;
    mouse.inputEventHandler = nullptr;
    mouse.used = false;

    RsInputDevice &pad = RsGlobal.pad;
    pad.inputDeviceType = rsPAD;
    pad.inputEventHandler = nullptr;
    pad.used = false;

    return psInitialize();
}

bool RsInitialize::Attach()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&functionAddress);
    auto pDetour   = reinterpret_cast<PVOID>(&Function);
    DetourAttach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}

bool RsInitialize::Detach()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&functionAddress);
    auto pDetour   = reinterpret_cast<PVOID>(&Function);
    DetourDetach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}
