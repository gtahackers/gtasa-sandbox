#include "RsMouseSetPos.h"
#include "../missing-in-plugin-sdk/Variables.h"
#include "../Globals.h"
#include <Windows.h>
#include <detours.h>
#include <cstdint>
#include <game_sa/RenderWare.h>

// 0x6194A0 ; RwInt32 __cdecl RsMouseSetPos(RwV2d *)
static auto Function = reinterpret_cast<RwInt32 (__cdecl *)(RwV2d *)>(0x6194A0);

static RwInt32 Hook(RwV2d * position)
{
    if (ForegroundApp && !ForegroundMenu)
        return Function(position);
    else
        return FALSE;
}

bool RsMouseSetPos::Attach()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<void **>(&Function);
    auto pDetour   = reinterpret_cast<void *>(&Hook);
    DetourAttach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}

bool RsMouseSetPos::Detach()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<void **>(&Function);
    auto pDetour   = reinterpret_cast<void *>(&Hook);
    DetourDetach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}
