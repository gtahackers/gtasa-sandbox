#include "Patches.h"
#include <iostream>
#include <Windows.h>
#include <detours.h>

// 0x53DED0 ; void __cdecl LoadingScreen(char const *, char const *)
static auto LoadingScreen = reinterpret_cast<void (__cdecl *)(char const *, char const *)>(0x53DED0);

static void __cdecl LoadingScreen_Hook(char const * source, char const * message);

void PrintLoadingScreenMessages()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&LoadingScreen);
    auto pDetour   = reinterpret_cast<PVOID>(&LoadingScreen_Hook);
    DetourAttach(ppPointer, pDetour);
    DetourTransactionCommit();
}

static void __cdecl LoadingScreen_Hook(char const * source, char const * message)
{
    if (!source || !message)
        return;
    
    std::cout << "[" << source << "] " << message << "\n";
}
