#include "Patches.h"
#include <Windows.h>
#include <cstring> // std::memcpy, std::memset

void EraseForcedFullscreen()
{
    // Before: 0x74887B | 39 1D 18 21 C9 00 | cmp ds:bMultiAdapter, ebx
    // After:  0x74887B | EB 5D 90 90 90 90 | jmp 07488DAh (short, relative)
    DWORD originalProtect;
    auto const address = reinterpret_cast<void *>(0x74887B);
    unsigned char const instructions[2] = { 0xEB, 0x5D };
    VirtualProtect(address, 6, PAGE_READWRITE, &originalProtect);
    std::memset(address, 0x90, 6);
    std::memcpy(address, instructions, 2);
    VirtualProtect(address, 6, originalProtect, &originalProtect);
}
