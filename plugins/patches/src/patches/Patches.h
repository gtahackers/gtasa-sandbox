#pragma once

void ForceStartGame();
void PrintLoadingScreenMessages();
void EraseForcedFullscreen();
void UseDirect3D9Proxy();
void NullCheckForAsciiToGxtChar();
void EraseForegroundCheckInGameLoop();
void DisableMainMenu();
