#include "Patches.h"
#include <iostream>
#include <Windows.h>
#include <timeapi.h>
#include <detours.h>
#include <d3dx9.h>
#include <thread>
#include "../missing-in-plugin-sdk/Variables.h"
#include "../missing-in-plugin-sdk/CVolumetricShadowMgr.h"
#include "../missing-in-plugin-sdk/CAudioEngine.h"
#include "../missing-in-plugin-sdk/CColAccel.h"
#include "../missing-in-plugin-sdk/CShopping.h"
#include <game_sa/CGame.h>
#include <game_sa/CFileLoader.h>
#include <game_sa/CCamera.h>
#include <game_sa/CLoadingScreen.h>
#include <game_sa/CStreaming.h>
#include <game_sa/CPed.h>
#include <game_sa/CPedIntelligence.h>
#include <game_sa/CPlayerPed.h>
#include <game_sa/CTask.h>
#include <game_sa/CPlaceable.h>
#include <game_sa/CPlayerInfo.h>
#include <game_sa/CEntity.h>
#include <game_sa/CWorld.h>
#include <game_sa/CTimer.h>
#include <game_sa/CTaskSimplePlayerOnFoot.h>
#include <game_sa/CClothes.h>
#include <game_sa/CStats.h>
#include <game_sa/CPedClothesDesc.h>
#include <game_sa/CKeyGen.h>

static auto CGamma__Init = reinterpret_cast<void (__thiscall *)(void *)>(0x747180);

static void PrepareGameStart();
static void InitialiseGame();
static void CreatePlayer(std::size_t playerID, float x, float y, float z);
static void GivePlayerClothes(std::size_t playerID, char const *group, char const *model, eClothesTexturePart bodyPart);

static void __fastcall CGamma__Init_Hook(void* self)
{
    CGamma__Init(self);
    PrepareGameStart();
}

void ForceStartGame()
{
    // Before the main game-loop in WinMain, there is a 'CGamma::Init(void)' call, which we will hook.
    // In the hook we execute the hooked function and run our initialization code, which in turn
    // starts the game without any movie, splash or the main menu.

    // 0x748710 = int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
    // 0x748A23 | B9 34 21 C9 00 | mov ecx, offset g_GameGamma
    // 0x748A28 | ...
    // 0x748A30 | E8 4B E7 FF FF | call CGamma::Init(void)

    // 0x747180 = void __thiscall CGamma::Init(void)
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&CGamma__Init);
    auto pDetour   = reinterpret_cast<PVOID>(&CGamma__Init_Hook);
    DetourAttach(ppPointer, pDetour);
    DetourTransactionCommit();
}

static void PrepareGameStart()
{
    if (gGameState != 0)
        return;
    
    // Push window to the front
    SetActiveWindow(RsGlobal.ps->window);
    SetForegroundWindow(RsGlobal.ps->window);

    // Show a random loading screen
    CLoadingScreen::m_currDisplayedSplash = 1;
    CLoadingScreen::LoadSplashes(0, 0);
    CLoadingScreen::DisplayNextSplash();

    { // Game state: 5
        if (!CGame::InitialiseEssentialsAfterRW()) {
            bShouldExitGame = TRUE;
            return;
        }

        CGame::InitialiseCoreDataAfterRW();

        auto const d3d9Caps = reinterpret_cast<D3DCAPS9 const *>(RwD3D9GetCaps());
        AnisoAvailable = (d3d9Caps->RasterCaps & D3DPRASTERCAPS_ANISOTROPY) == D3DPRASTERCAPS_ANISOTROPY;
    }

    { // Game state: 6
        IsInMenu = false;
        *reinterpret_cast<bool *>(0xBA6831) = true;
        CTimer::m_CodePause = false;
        CTimer::m_UserPause = false;

        if (*reinterpret_cast<DWORD *>(0x8D6218)) {
            *reinterpret_cast<DWORD *>(0xBA681C) = curVideoMode;
            savedVideoMode = curVideoMode;
            *reinterpret_cast<DWORD *>(0x8D6218) = 0;
        }
    }

    { // Game state: 8
        CAudioEngine::SetEffectsFaderScalingFactor(&AudioEngine, 1.0f);
        CStats__bShowUpdateStats = false;
        InitialiseGame();
        *reinterpret_cast<bool *>(0xBA6831) = 0;
        CAudioEngine::InitialisePostLoading(&AudioEngine);
    }

    gGameState = 9;
}

static void InitialiseGame()
{
    char const * szGameLevel = "DATA\\GTA.DAT";

    CGame::Init1(szGameLevel);
    CColAccel::startCache();
    CFileLoader::LoadLevel("DATA\\DEFAULT.DAT");
    CFileLoader::LoadLevel(szGameLevel);
    CColAccel::endCache();
    CGame::Init2(szGameLevel);
    CVolumetricShadowMgr::Initialise();

    CStats::SetStatValue(STAT_ENERGY, 800.0);
    CStats::SetStatValue(STAT_MUSCLE, 50.0);
    CStats::SetStatValue(STAT_FAT, 200.0);
    CStats::SetStatValue(STAT_DRIVING_SKILL, 0.0);
    CStats::SetStatValue(STAT_CITY_UNLOCKED, 0);
    CStats::SetStatValue(STAT_RESPECT, 0);
    CreatePlayer(0, 2488.562f, -1666.865f, 12.8757f);
    GivePlayerClothes(0, "VEST", "VEST", CLOTHES_TEXTURE_TORSO);
    GivePlayerClothes(0, "JEANSDENIM", "JEANS", CLOTHES_TEXTURE_LEGS);
    GivePlayerClothes(0, "SNEAKERBINCBLK", "SNEAKER", CLOTHES_TEXTURE_SHOES);
    GivePlayerClothes(0, "PLAYER_FACE", "HEAD", CLOTHES_TEXTURE_HEAD);
    CClothes::RebuildPlayer(CWorld::Players[0].m_pPed, false);

    TheCamera.Process();
    CGame::Init3(szGameLevel);
    CAudioEngine::Restart(&AudioEngine);

    if (*reinterpret_cast<bool *>(0xBA67A8)) {
        *reinterpret_cast<bool *>(0xBA67A5) = true;
    }
}

static void CreatePlayer(std::size_t playerID, float x, float y, float z)
{
    if (playerID > MAX_PLAYERS)
        return;
    
    if (*reinterpret_cast<bool *>(0x8E4CD0) != 1) {
        char name[] = "player";
        CStreaming::RequestSpecialModel(0, name, 26);
        CStreaming::LoadAllRequestedModels(true);
    }

    CPlayerPed::SetupPlayerPed(playerID);

    CPlayerInfo& playerInfo = CWorld::Players[playerID];
    CPed * const pPed = playerInfo.m_pPed;

    pPed->SetCharCreatedBy(2);
    CPlayerPed::DeactivatePlayerPed(playerID);
    
    if (z <= -100.0f) {
        z = CWorld::FindGroundZForCoord(x, y);
        z += pPed->GetDistanceFromCentreOfMassToBaseOfModel();
    }

    pPed->SetPosn(x, y, z);
    CPlayerPed::ReactivatePlayerPed(playerID);

    auto pTaskSimplePlayerOnFoot = new CTaskSimplePlayerOnFoot();

    if (pTaskSimplePlayerOnFoot) {
        pPed->m_pIntelligence->m_TaskMgr.SetTask(pTaskSimplePlayerOnFoot, 4, false);
    }
}

static void GivePlayerClothes(std::size_t playerID, char const *group, char const *model, eClothesTexturePart bodyPart)
{
    if (playerID > MAX_PLAYERS)
        return;

    CPlayerInfo& playerInfo = CWorld::Players[playerID];
    playerInfo.m_PlayerData.m_pPedClothesDesc->SetTextureAndModel(group, model, bodyPart);
    CShopping::SetPlayerHasBought(CKeyGen::GetUppercaseKey(group));
}
