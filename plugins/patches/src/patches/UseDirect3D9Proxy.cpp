#include "Patches.h"
#include <iostream>
#include <Windows.h>
#include <detours.h>
#include <cstdint>
#include <cstring>
#include "Direct3D9Proxy.h"

using Direct3DCreate9_t = IDirect3D9 *(WINAPI *)(unsigned int);
static Direct3DCreate9_t Direct3DCreate9_Game = nullptr;

static IDirect3D9* WINAPI Direct3DCreate9_Hook(unsigned int SDKVersion)
{
    IDirect3D9* pDirect3D9 = Direct3DCreate9_Game(SDKVersion);

    if (!pDirect3D9)
        return nullptr;

    return new Direct3D9Proxy(pDirect3D9);
}

void UseDirect3D9Proxy()
{
    Direct3DCreate9_Game = reinterpret_cast<Direct3DCreate9_t>(DetourFindFunction("d3d9.dll", "Direct3DCreate9"));

    if (!Direct3DCreate9_Game)
        return;
    
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    DetourAttach((PVOID *)&Direct3DCreate9_Game, (PVOID)Direct3DCreate9_Hook);
    DetourTransactionCommit();
}
