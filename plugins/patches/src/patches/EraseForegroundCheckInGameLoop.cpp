#include "Patches.h"
#include <cstring> // std::memset
#include <Windows.h>

void EraseForegroundCheckInGameLoop()
{
    //     0x748719 | 33 DB             | xor ebx, ebx
    //     ...
    //     0x748A87 | 39 1D 1C 62 8D 00 | cmp ds:ForegroundApp, ebx
    // >>> 0x748A8D | 0F 84 20 03 00 00 | jz loc_748DB3
    DWORD dwOldProtect;
    auto address = reinterpret_cast<void *>(0x748A8D);
    VirtualProtect(address, 6, PAGE_READWRITE, &dwOldProtect);
    std::memset(address, 0x90, 6);
    VirtualProtect(address, 6, dwOldProtect, &dwOldProtect);
}
