#include "Patches.h"
#include <cstring> // std::memset
#include <Windows.h>

void DisableMainMenu()
{
    // >>> 0x53BF44 | E8 F7 F4 03 00 | call CMenuManager::Process(void)
    DWORD dwOldProtect;
    auto address = reinterpret_cast<void *>(0x53BF44);
    VirtualProtect(address, 5, PAGE_READWRITE, &dwOldProtect);
    std::memset(address, 0x90, 5);
    VirtualProtect(address, 5, dwOldProtect, &dwOldProtect);
}
