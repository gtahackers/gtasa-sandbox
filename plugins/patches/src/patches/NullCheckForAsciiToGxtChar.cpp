#include "Patches.h"
#include <Windows.h>
#include <detours.h>

// 0x718600 ; void __cdecl AsciiToGxtChar(char const *, unsigned char *)
static auto AsciiToGxtChar = reinterpret_cast<void (__cdecl *)(char const *, char const *)>(0x718600);

static void __cdecl AsciiToGxtChar_Hook(char const * a, char const * b)
{
    if (!a || !b)
        return;
    
    return AsciiToGxtChar(a, b);
}

void NullCheckForAsciiToGxtChar()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&AsciiToGxtChar);
    auto pDetour   = reinterpret_cast<PVOID>(&AsciiToGxtChar_Hook);
    DetourAttach(ppPointer, pDetour);
    DetourTransactionCommit();
}
