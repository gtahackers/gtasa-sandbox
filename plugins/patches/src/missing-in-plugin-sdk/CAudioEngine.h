#pragma once

#include <cstdint>

class CAudioEngine
{
    using StartLoadingTune_t = void (__thiscall *)(CAudioEngine *);
    using InitialisePostLoading_t = void (__thiscall *)(CAudioEngine *);
    using Restart_t = void (__thiscall *)(CAudioEngine *);
    using Reset_t = void (__thiscall *)(CAudioEngine *);
    using ResumeAllSounds_t = void (__thiscall *)(CAudioEngine *);
    using PauseManually_t = void (__thiscall *)(CAudioEngine *, bool);
    using ServiceLoadingTune_t = void (__thiscall *)(CAudioEngine *, float);
    using SetMusicMasterVolume_t = void (__thiscall *)(CAudioEngine *, std::int8_t);
    using SetEffectsMasterVolume_t = void (__thiscall *)(CAudioEngine *, std::int8_t);
    using SetMusicFaderScalingFactor_t = void (__thiscall *)(CAudioEngine *, float);
    using SetEffectsFaderScalingFactor_t = void (__thiscall *)(CAudioEngine *, float);
    using SetStreamFaderScalingFactor_t = void (__thiscall *)(CAudioEngine *, float);
    using SetNonStreamFaderScalingFactor_t = void (__thiscall *)(CAudioEngine *, float);

public:
    // 0x507410 ; void __thiscall CAudioEngine::StartLoadingTune(void)
    static inline StartLoadingTune_t StartLoadingTune = reinterpret_cast<StartLoadingTune_t>(0x507410);

    // 0x5078F0 ; void __thiscall CAudioEngine::InitialisePostLoading(void)
    static inline InitialisePostLoading_t InitialisePostLoading = reinterpret_cast<InitialisePostLoading_t>(0x5078F0);

    // 0x506DB0 ; void __thiscall CAudioEngine::Restart(void)
    static inline Restart_t Restart = reinterpret_cast<Restart_t>(0x506DB0);

    // 0x507A90 ; void __thiscall CAudioEngine::Reset(void)
    static inline Reset_t Reset = reinterpret_cast<Reset_t>(0x507A90);

    // 0x507440 ; void __thiscall CAudioEngine::ResumeAllSounds(void)
    static inline ResumeAllSounds_t ResumeAllSounds = reinterpret_cast<ResumeAllSounds_t>(0x507440);

    // 0x4EF510 ; void __thiscall CAESoundManager::PauseManually(bool)
    static inline PauseManually_t PauseManually = reinterpret_cast<PauseManually_t>(0x4EF510);

    // 0x5078A0 ; void __thiscall CAudioEngine::ServiceLoadingTune(float)
    static inline ServiceLoadingTune_t ServiceLoadingTune = reinterpret_cast<ServiceLoadingTune_t>(0x5078A0);

    // 0x506DE0 ; void __thiscall CAudioEngine::SetMusicMasterVolume(signed char)
    static inline SetMusicMasterVolume_t SetMusicMasterVolume = reinterpret_cast<SetMusicMasterVolume_t>(0x506DE0);

    // 0x506E10 ; void __thiscall CAudioEngine::SetEffectsMasterVolume(signed char)
    static inline SetEffectsMasterVolume_t SetEffectsMasterVolume = reinterpret_cast<SetEffectsMasterVolume_t>(0x506E10);

    // 0x506E40 ; void __thiscall CAudioEngine::SetMusicFaderScalingFactor(float)
    static inline SetMusicFaderScalingFactor_t SetMusicFaderScalingFactor = reinterpret_cast<SetMusicFaderScalingFactor_t>(0x506E40);

    // 0x506E50 ; void __thiscall CAudioEngine::SetEffectsFaderScalingFactor(float)
    static inline SetEffectsFaderScalingFactor_t SetEffectsFaderScalingFactor = reinterpret_cast<SetEffectsFaderScalingFactor_t>(0x506E50);

    // 0x506E60 ; void __thiscall CAudioEngine::SetStreamFaderScalingFactor(float)
    static inline SetStreamFaderScalingFactor_t SetStreamFaderScalingFactor = reinterpret_cast<SetStreamFaderScalingFactor_t>(0x506E60);

    // 0x506E70 ; void __thiscall CAudioEngine::SetNonStreamFaderScalingFactor(float)
    static inline SetNonStreamFaderScalingFactor_t SetNonStreamFaderScalingFactor = reinterpret_cast<SetNonStreamFaderScalingFactor_t>(0x506E70);
};

// 0xB6BC90 ; class CAudioEngine AudioEngine
static auto& AudioEngine = *reinterpret_cast<CAudioEngine *>(0xB6BC90);
