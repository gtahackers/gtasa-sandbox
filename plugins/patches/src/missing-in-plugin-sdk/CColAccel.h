#pragma once

class CColAccel
{
    using startCache_t = void (__cdecl *)();
    using endCache_t = void (__cdecl *)();

public:
    // 0x5B31A0 ; static void __cdecl CColAccel::startCache(void)
    static inline startCache_t startCache = reinterpret_cast<startCache_t>(0x5B31A0);

    // 0x05B2AD0 ; static void __cdecl CColAccel::endCache(void)
    static inline endCache_t endCache = reinterpret_cast<endCache_t>(0x05B2AD0);
};
