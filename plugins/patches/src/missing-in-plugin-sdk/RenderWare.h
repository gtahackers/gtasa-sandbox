#pragma once

#include <cstdint>
#include <game_sa/RenderWare.h>

// 0x619B60 ; RsEventStatus __cdecl RsEventHandler(RsEvent event, void * param)
using RsEventHandler_t = RsEventStatus (__cdecl *)(int, std::intptr_t);
static auto RsEventHandler = reinterpret_cast<RsEventHandler_t>(0x619B60);

// 0x747820 ; bool __cdecl PopulateRwKeyCode(LPARAM *, LPARAM lParam, WPARAM wParam)
using PopulateRwKeyCode_t = bool (__cdecl *)(LPARAM *, LPARAM, WPARAM);
static auto PopulateRwKeyCode = reinterpret_cast<PopulateRwKeyCode_t>(0x747820);

// 0x619560 ; RsEventStatus __cdecl RsKeyboardEventHandler(int, LPARAM *);
using RsKeyboardEventHandler_t = RsEventStatus (__cdecl *)(std::int32_t, void *);
static auto RsKeyboardEventHandler = reinterpret_cast<RsKeyboardEventHandler_t>(0x619560);

// 0x619580 ; RsEventStatus __cdecl RsMouseEventHandler(RsEvent, void *);
using RsMouseEventHandler_t = RsEventStatus (__cdecl *)(std::int32_t, void *);
static auto RsMouseEventHandler = reinterpret_cast<RsMouseEventHandler_t>(0x619580);

// 0x747CD0 ; void __cdecl HandleAnySpecialVirtualKeyChecks(LPARAM *);
using HandleAnySpecialVirtualKeyChecks_t = bool (__cdecl *)(LPARAM *);
static auto HandleAnySpecialVirtualKeyChecks = reinterpret_cast<HandleAnySpecialVirtualKeyChecks_t>(0x747CD0);

// 0x747000 ; void __cdecl FreeAllDirectInputObjects()
using FreeAllDirectInputObjects_t = void (__cdecl *)();
static auto FreeAllDirectInputObjects = reinterpret_cast<FreeAllDirectInputObjects_t>(0x747000);

// 0x747420 ; RwInt32 __cdecl psInitialize()
using psInitialize_t = std::int32_t (__cdecl *)();
static auto psInitialize = reinterpret_cast<psInitialize_t>(0x747420);
