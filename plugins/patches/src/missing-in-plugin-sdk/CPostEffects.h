#pragma once

#include <cstdint>

class CPostEffects
{
    using SetupBackBufferVertex_t = void (__cdecl *)();

public:
    // 0x7043D0 ; static void __cdecl CPostEffects::SetupBackBufferVertex(void)
    static inline SetupBackBufferVertex_t SetupBackBufferVertex = reinterpret_cast<SetupBackBufferVertex_t>(0x7043D0);
};
