#pragma once

#include <cstdint>

class CShopping
{
    using SetPlayerHasBought_t = void (__cdecl *)(std::uint32_t);

public:
    // 0x49B610 ; static void __cdecl CShopping::SetPlayerHasBought(unsigned int)
    static inline SetPlayerHasBought_t SetPlayerHasBought = reinterpret_cast<SetPlayerHasBought_t>(0x49B610);
};
