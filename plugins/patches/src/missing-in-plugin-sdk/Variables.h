#pragma once

#include <cstdint>

// 0xC87FFC ; bool AnisoAvailable
static auto& AnisoAvailable = *reinterpret_cast<bool *>(0xC87FFC);

// 0xC8D4C0 ; unsigned int gGameState
static auto& gGameState = *reinterpret_cast<std::uint32_t *>(0xC8D4C0);

// 0xC17050 ; DWORD bShouldExitGame
static auto& bShouldExitGame = *reinterpret_cast<std::uint32_t *>(0xC17050);

// 0xBA677B ; bool IsInMenu
static auto& IsInMenu = *reinterpret_cast<bool *>(0xBA677B);

// 0x8D6220 ; RwInt32 curVideoMode
static auto& curVideoMode = *reinterpret_cast<std::uint32_t *>(0x8D6220);

// 0x8D6248; RwInt32 curSubSystem
static auto& curSubSystem = *reinterpret_cast<std::uint32_t *>(0x8D6248);

// 0xBA6820 ; RwInt32 savedVideoMode
static auto& savedVideoMode = *reinterpret_cast<std::uint32_t *>(0xBA6820);

// 0x8CDE56 ; static unsigned char CStats::bShowUpdateStats
static auto& CStats__bShowUpdateStats = *reinterpret_cast<bool *>(0x8CDE56);

// 0xC920E8 ; BOOL RwInitialized
static auto& RwInitialized = *reinterpret_cast<bool *>(0xC920E8);

// 0x8D621C ; BOOL ForegroundApp
static auto& ForegroundApp = *reinterpret_cast<bool *>(0x8D621C);
