#pragma once

class CVolumetricShadowMgr
{
    using Initialise_t = void (__cdecl *)();

public:
    // 0x70F9E0 ; static void __cdecl CVolumetricShadowMgr::Initialise(void)
    static inline Initialise_t Initialise = reinterpret_cast<Initialise_t>(0x70F9E0);
};
