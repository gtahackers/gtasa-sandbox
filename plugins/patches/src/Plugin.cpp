#include "patches/Patches.h"
#include "overwrite/psSelectDevice.h"
#include "overwrite/RsInitialize.h"
#include "overwrite/MainWndProc.h"
#include "overwrite/RsMouseSetPos.h"
#include "overwrite/IsForegroundApp.h"
#include <plugin_manager/Plugin.h>

PLUGIN_DECL void PLUGIN_API OnPluginStart(void)
{
    PrintLoadingScreenMessages();
    EraseForegroundCheckInGameLoop();
    EraseForcedFullscreen();
    ForceStartGame();
    UseDirect3D9Proxy();
    DisableMainMenu();
    NullCheckForAsciiToGxtChar();
    psSelectDevice::Attach();
    RsInitialize::Attach();
    MainWndProc::Attach();
    RsMouseSetPos::Attach();
    IsForegroundApp::Attach();
}

PLUGIN_DECL void PLUGIN_API OnPluginStop(void)
{
    IsForegroundApp::Detach();
    RsMouseSetPos::Detach();
    MainWndProc::Detach();
    RsInitialize::Detach();
    psSelectDevice::Detach();
}
