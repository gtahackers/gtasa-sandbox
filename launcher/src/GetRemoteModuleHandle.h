#pragma once

#include <minwindef.h>

#ifdef UNICODE
#   define GetRemoteModuleHandle GetRemoteModuleHandleW
#else
#   define GetRemoteModuleHandle GetRemoteModuleHandleA
#endif

HMODULE WINAPI GetRemoteModuleHandleA(HANDLE hProcess, LPCSTR lpModuleName);
HMODULE WINAPI GetRemoteModuleHandleW(HANDLE hProcess, LPCWSTR lpModuleName);
