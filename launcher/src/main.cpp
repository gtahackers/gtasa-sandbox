#include "Launcher.h"
#include "GetCurrentProcessPath.h"
#include <iostream>
#include <thread>

int wmain(int argc, wchar_t *argv[])
{
    const fs::path launcher = GetCurrentProcessPath();

    if (argc < 2) {
        std::wcerr << "Usage: " << launcher.filename().wstring() << " <path to gta_sa.exe>\n";
        return EXIT_FAILURE;
    }

    bool waitForExit = false;
    const wchar_t* gameExecutablePath = nullptr;

    if (argc >= 3) {
        if (!std::wcscmp(argv[1], L"--wait")) {
            gameExecutablePath = argv[2];
            waitForExit = true;
        }
        else if (!std::wcscmp(argv[2], L"--wait")) {
            gameExecutablePath = argv[1];
            waitForExit = true;
        }
    }
    else {
        gameExecutablePath = argv[1];
    }

    std::wcout << "launcher = " << launcher.wstring() << "\n";

    fs::path gameExecutable(gameExecutablePath);
    StartGame(gameExecutable, launcher.parent_path(), waitForExit);
    
    if (waitForExit)
        std::this_thread::sleep_for(std::chrono::seconds(5));
    
    return EXIT_SUCCESS;
}
