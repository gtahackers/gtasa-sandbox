#pragma once

#include <string>
#include <type_traits>
#include <limits>
#include <minwindef.h>

class RemoteModule
{
public:
    RemoteModule(const char* moduleName = nullptr);
    RemoteModule(const wchar_t* moduleName = nullptr);
    RemoteModule(const std::string& moduleName);
    RemoteModule(const std::wstring& moduleName);
    RemoteModule(HANDLE hProcess, const char* moduleName);
    RemoteModule(HANDLE hProcess, const wchar_t* moduleName);
    RemoteModule(HANDLE hProcess, const std::string& moduleName);
    RemoteModule(HANDLE hProcess, const std::wstring& moduleName);
    RemoteModule(HMODULE hModule);
    RemoteModule(HANDLE hProcess, HMODULE hModule);

    template <typename ReturnType = DWORD>
    ReturnType Call(const char* functionName, const void* argument = nullptr, const std::size_t size = 0)
    {
        DWORD value = CallInternal(functionName, argument, size);
        return *reinterpret_cast<ReturnType *>(reinterpret_cast<void *>(&value));
    }

    template <typename ReturnType = DWORD, typename T, std::size_t N>
    ReturnType Call(const char* functionName, T (&argument)[N])
    {
        return Call<ReturnType>(functionName, &argument[0], N);
    }

    template <typename ReturnType = DWORD>
    ReturnType Call(const char* functionName, const std::string& argument)
    {
        return Call<ReturnType>(functionName, argument.c_str(), argument.size() + 1);
    }

    template <typename ReturnType = DWORD>
    ReturnType Call(const char* functionName, const std::wstring& argument)
    {
        return Call<ReturnType>(functionName, argument.c_str(), argument.size() + 1);
    }

    template <typename ReturnType = DWORD>
    ReturnType CallShared(HANDLE hProcess, const char* functionName, const void* argument = nullptr, const std::size_t size = 0)
    {
        DWORD value = CallSharedInternal(hProcess, functionName, argument, size);
        return *reinterpret_cast<ReturnType *>(reinterpret_cast<void *>(&value));
    }

    template <typename ReturnType = DWORD, typename T, std::size_t N>
    ReturnType CallShared(HANDLE hProcess, const char* functionName, T (&argument)[N])
    {
        return CallShared<ReturnType>(hProcess, functionName, &argument[0], N);
    }

    template <typename ReturnType = DWORD>
    ReturnType CallShared(HANDLE hProcess, const char* functionName, const std::string& argument)
    {
        return CallShared<ReturnType>(hProcess, functionName, argument.c_str(), argument.size() + 1);
    }

    template <typename ReturnType = DWORD>
    ReturnType CallShared(HANDLE hProcess, const char* functionName, const std::wstring& argument)
    {
        return CallShared<ReturnType>(hProcess, functionName, argument.c_str(), argument.size() + 1);
    }

private:
    DWORD CallInternal(const char* functionName, const void* argument, const std::size_t size);
    DWORD CallSharedInternal(HANDLE hProcess, const char* functionName, const void* argument, const std::size_t size);
    DWORD CallRemoteFunction(HANDLE hProcess, FARPROC pFunction, const void* argument, const std::size_t size);

    HANDLE  m_hProcess;
    HMODULE m_hModule;
};
