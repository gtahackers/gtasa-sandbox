#pragma once

#include <filesystem>

namespace fs = std::experimental::filesystem;

bool StartGame(const fs::path& executable, const fs::path& launcherFolder, bool waitForExit);
