#include "GetCurrentProcessPath.h"
#include <Windows.h>

fs::path GetCurrentProcessPath()
{
    TCHAR launcherPath[MAX_PATH];
    HMODULE hModule = GetModuleHandle(NULL);
    GetModuleFileName(hModule, launcherPath, MAX_PATH);
    return fs::path(launcherPath);
}
