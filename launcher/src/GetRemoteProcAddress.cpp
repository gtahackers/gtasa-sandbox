#include "GetRemoteProcAddress.h"
#include <memory>
#include <string>
#include <cstdint>
#include <Windows.h>
#include <Psapi.h>

FARPROC WINAPI GetRemoteProcAddress(HANDLE hProcess, HMODULE hModule, LPCSTR lpProcName)
{
    if (!lpProcName)
        return NULL;

    MODULEINFO moduleInfo = { 0 };
    
    if (!GetModuleInformation(hProcess, hModule, &moduleInfo, sizeof(moduleInfo)))
        return NULL;

    IMAGE_DOS_HEADER dosHeader         = { 0 };
    LPVOID           pModuleBase       = moduleInfo.lpBaseOfDll;
    auto             moduleBaseAddress = reinterpret_cast<std::intptr_t>(pModuleBase);

    if (!ReadProcessMemory(hProcess, pModuleBase, &dosHeader, sizeof(dosHeader), NULL))
        return NULL;
    
    if (dosHeader.e_magic != IMAGE_DOS_SIGNATURE)
        return NULL;

    DWORD         signature        = 0;
    std::intptr_t signatureAddress = moduleBaseAddress + dosHeader.e_lfanew;
    auto          pSignature       = reinterpret_cast<LPVOID>(signatureAddress);

    if (!ReadProcessMemory(hProcess, pSignature, &signature, sizeof(signature), NULL))
        return NULL;

    if (signature != IMAGE_NT_SIGNATURE)
        return NULL;

    IMAGE_FILE_HEADER fileHeader        = { 0 };
    std::intptr_t     fileHeaderAddress = signatureAddress + sizeof(signature);
    auto              pFileHeader       = reinterpret_cast<LPVOID>(fileHeaderAddress);

    if (!ReadProcessMemory(hProcess, pFileHeader, &fileHeader, sizeof(fileHeader), NULL))
        return NULL;

    if (fileHeader.Machine != IMAGE_FILE_MACHINE_I386 || fileHeader.SizeOfOptionalHeader != sizeof(IMAGE_OPTIONAL_HEADER32))
        return NULL;

    IMAGE_OPTIONAL_HEADER32 optHeader         = { 0 };
    std::intptr_t           optHeaderAddress  = fileHeaderAddress + sizeof(fileHeader);
    auto                    pOptHeader        = reinterpret_cast<LPVOID>(optHeaderAddress);

    if (!ReadProcessMemory(hProcess, pOptHeader, &optHeader, sizeof(optHeader), NULL))
        return NULL;

    if (optHeader.NumberOfRvaAndSizes <= IMAGE_DIRECTORY_ENTRY_EXPORT)
        return NULL;

    IMAGE_DATA_DIRECTORY&  exportDirectory    = optHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
    IMAGE_EXPORT_DIRECTORY exportTable        = { 0 };
    std::intptr_t          exportTableAddress = moduleBaseAddress + exportDirectory.VirtualAddress;
    auto                   pExportTable       = reinterpret_cast<LPVOID>(exportTableAddress);

    if (!ReadProcessMemory(hProcess, pExportTable, &exportTable, sizeof(exportTable), NULL))
        return NULL;

    std::intptr_t functionsAddress = moduleBaseAddress + exportTable.AddressOfFunctions;
    std::intptr_t namesAddress     = moduleBaseAddress + exportTable.AddressOfNames;
    // std::intptr_t nameOrdinalsAddress = moduleBaseAddress + exportTable.AddressOfNameOrdinals;

    auto pFunctionsAddress = reinterpret_cast<LPVOID>(functionsAddress);
    auto pNamesAddress     = reinterpret_cast<LPVOID>(namesAddress);
    // auto pNameOrdinalsAddress = reinterpret_cast<LPVOID>(nameOrdinalsAddress);

    auto pFunctions = std::make_unique<DWORD[]>(exportTable.NumberOfFunctions);
    auto pNames     = std::make_unique<DWORD[]>(exportTable.NumberOfNames);
    // auto pNameOrdinals = std::make_unique<DWORD[]>(exportTable.NumberOfNames);

    if (!ReadProcessMemory(hProcess, pFunctionsAddress, pFunctions.get(), exportTable.NumberOfFunctions * sizeof(DWORD), NULL))
        return NULL;

    if (!ReadProcessMemory(hProcess, pNamesAddress, pNames.get(), exportTable.NumberOfNames * sizeof(DWORD), NULL))
        return NULL;

    // if (!ReadProcessMemory(hProcess, pNameOrdinalsAddress, pNameOrdinals.get(), exportTable.NumberOfNames * sizeof(DWORD), NULL))
    //     return NULL;

    std::string functionName;
    functionName.reserve(256);

    for (std::size_t i = 0; i < exportTable.NumberOfNames; ++i)
    {
        functionName.clear();

        bool nameReadingProblem = false;
        std::intptr_t nameBaseAddress = moduleBaseAddress + pNames[i];

        for (std::ptrdiff_t offset = 0;; ++offset)
        {
            char c;
            auto pCharacter = reinterpret_cast<LPVOID>(nameBaseAddress + offset);
            
            if (!ReadProcessMemory(hProcess, pCharacter, &c, sizeof(c), NULL)) {
                nameReadingProblem = true;
                break;
            }

            functionName.push_back(c);

            if (c == '\0')
                break;
        }

        if (nameReadingProblem)
            continue;

        if (functionName.find(lpProcName) == std::string::npos)
            continue;

        std::intptr_t procAddress = moduleBaseAddress + pFunctions[i];
        return reinterpret_cast<FARPROC>(procAddress);
    }

    return NULL;
}
