#include "GetRemoteModuleHandle.h"
#include <cstring>
#include <cwchar>
#include <cctype>
#include <Windows.h>
#include <TlHelp32.h>
#include <strsafe.h>

HMODULE WINAPI GetRemoteModuleHandleA(HANDLE hProcess, LPCSTR lpModuleName)
{
    if (!lpModuleName)
        return NULL;
    
    const int numWideChars = MultiByteToWideChar(CP_UTF8, 0, lpModuleName, -1, NULL, 0);
    auto szWideModuleName = new wchar_t[numWideChars];
    MultiByteToWideChar(CP_UTF8, 0, lpModuleName, -1, szWideModuleName, numWideChars);
    HMODULE ret = GetRemoteModuleHandleW(hProcess, szWideModuleName);
    delete[] szWideModuleName;
    return ret;
}

HMODULE WINAPI GetRemoteModuleHandleW(HANDLE hProcess, LPCWSTR lpModuleName)
{
    if (!lpModuleName)
        return NULL;
    
    std::size_t moduleNameLength = 0;

    if (FAILED(StringCchLength(lpModuleName, MAX_PATH, &moduleNameLength)))
        return NULL;

    TCHAR szModuleName[MAX_PATH] = { 0 };

    if (FAILED(StringCchCopy(szModuleName, MAX_PATH, lpModuleName)))
        return NULL;

    CharLower(szModuleName);

    HANDLE hModuleSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetProcessId(hProcess));

    if (hModuleSnapshot == INVALID_HANDLE_VALUE)
        return NULL;

    MODULEENTRY32 moduleEntry = { 0 };
    moduleEntry.dwSize = sizeof(moduleEntry);

    if (!Module32First(hModuleSnapshot, &moduleEntry)) {
        CloseHandle(hModuleSnapshot);
        return NULL;
    }

    do
    {
        CharLower(moduleEntry.szModule);

        if (!std::wcsstr(moduleEntry.szModule, szModuleName))
            continue;

        CloseHandle(hModuleSnapshot);
        return moduleEntry.hModule;
    }
    while (Module32Next(hModuleSnapshot, &moduleEntry));
    
    CloseHandle(hModuleSnapshot);
    return NULL;
}
