#include "Launcher.h"
#include "RemoteModule.h"
#include <iostream>
#include <sstream>
#include <string>
#include <Windows.h>
#include <plugin_manager/PluginManager.h>

static bool StartGameProcess(const fs::path& executable, PROCESS_INFORMATION* pProcessInfo);
static bool LoadPluginManager(HANDLE hProcess, const fs::path& dll);
static bool LaunchDebugger(DWORD const pid);

bool StartGame(const fs::path& executable, const fs::path& launcherFolder, bool waitForExit)
{
    const fs::path pluginManagerDLL = launcherFolder / "plugin_manager.dll";

    std::wcout << "executable                     = " << executable.wstring() << "\n";
    std::wcout << "executable.parent_path()       = " << executable.parent_path().wstring() << "\n";
    std::wcout << "pluginManagerDLL               = " << pluginManagerDLL.wstring() << "\n";
    std::wcout << "pluginManagerDLL.parent_path() = " << pluginManagerDLL.parent_path().wstring() << "\n";

    if (!fs::is_regular_file(pluginManagerDLL)) {
        std::wcerr << "StartGame: Error: Plugin manager library is not a regular file or not found\n";
        return false;
    }

    PROCESS_INFORMATION pi = { 0 };

    if (!StartGameProcess(executable, &pi)) {
        std::wcerr << "StartGame: Error: Failed to create the game process: " << GetLastError() << "\n";
        return false;
    }

    std::wcout << "Game has started (process: " << pi.dwProcessId << ", thread: " << pi.dwThreadId << ")\n";

    if (IsDebuggerPresent()) {
        std::wcout << "Debugger is present. Launching JIT debugger for game process\n";
        LaunchDebugger(pi.dwProcessId);
    }

    if (!LoadPluginManager(pi.hProcess, pluginManagerDLL)) {
        std::wcerr << "StartGame: Error: Failed to remote load the plugin manager into the game process\n";
        TerminateProcess(pi.hProcess, EXIT_FAILURE);
        CloseHandle(pi.hThread);
        CloseHandle(pi.hProcess);
        return false;
    }

    std::wcout << "Plugin manager has been loaded into the game process\n";

    ResumeThread(pi.hThread);

    if (waitForExit) {
        std::wcout << "Game main thread resumed. Waiting for exit\n";

        WaitForSingleObject(pi.hProcess, INFINITE);

        DWORD dwResult = 0;

        if (!GetExitCodeProcess(pi.hProcess, &dwResult)) {
            CloseHandle(pi.hThread);
            CloseHandle(pi.hProcess);
            std::wcerr << "GetExitCodeProcess failed: " << GetLastError() << "\n";
            return false;
        }

        std::wcout << "Game process exited with code: " << dwResult << "\n";
    }

    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);
    return true;
}

static bool StartGameProcess(const fs::path& executable, PROCESS_INFORMATION* pProcessInfo)
{
    STARTUPINFO startupInfo = { 0 };
    startupInfo.cb = sizeof(startupInfo);

    SECURITY_ATTRIBUTES processAttributes = { 0 };
    processAttributes.nLength = sizeof(processAttributes);

    SECURITY_ATTRIBUTES threadAttributes = { 0 };
    threadAttributes.nLength = sizeof(threadAttributes);

    const std::wstring applicationName  = executable.wstring();
    const std::wstring currentDirectory = executable.parent_path().wstring();

    const DWORD dwCreationFlags = CREATE_DEFAULT_ERROR_MODE
                                | CREATE_SUSPENDED
                                | CREATE_UNICODE_ENVIRONMENT;

    SetLastError(0);

    return CreateProcess(
        /* lpApplicationName     */ applicationName.c_str(), 
        /* lpCommandLine         */ NULL, 
        /* lpProcessAttributes   */ &processAttributes,
        /* lpThreadAttributes    */ &threadAttributes, 
        /* bInheritHandles       */ FALSE, 
        /* dwCreationFlags       */ dwCreationFlags,
        /* lpEnvironment         */ NULL, 
        /* lpCurrentDirectory    */ currentDirectory.c_str(), 
        /* lpStartupInfo         */ &startupInfo, 
        /* lpProcessInformation  */ pProcessInfo
    );
}

static bool LoadPluginManager(HANDLE hProcess, const fs::path& dll)
{
    std::string const pluginManagerFolder = dll.parent_path().string();

    RemoteModule Kernel32("kernel32");
    BOOL directoryPathAdded = Kernel32.CallShared<BOOL>(hProcess, "SetDllDirectoryA", pluginManagerFolder);

    const std::string pluginManagerName = dll.filename().string();
    std::string pluginManagerPath;

    if (!directoryPathAdded) {
        std::wcout << "LoadPluginManager: Note: Call to SetDllDirectory has failed. Loading plugin manager with absolute path\n";
        pluginManagerPath = dll.string();
    }
    else {
        pluginManagerPath = pluginManagerName;
    }

    HMODULE hPluginManagerModule = Kernel32.CallShared<HMODULE>(hProcess, "LoadLibraryA", pluginManagerPath);

    if (!hPluginManagerModule) {
        std::wcerr << "LoadPluginManager: Error: LoadLibrary returned NULL\n";
        return false;
    }

    PluginManager::InitData data = { 0 };
    strncpy_s(data.szDirectory, pluginManagerFolder.c_str(), pluginManagerFolder.size());

    RemoteModule PluginManager(hProcess, hPluginManagerModule);
    PluginManager.Call("OnPluginManagerStart", &data, sizeof(data));
    return true;
}

static bool LaunchDebugger(DWORD const pid)
{
    // Create path to vsjitdebugger.exe (usually in C:/Windows/system32)
    std::wstring systemDirPath(MAX_PATH + 1, '\0');
    UINT systemDirLength = GetSystemDirectoryW(&systemDirPath[0], systemDirPath.size());

    if (!systemDirLength)
        return false;

    if (systemDirLength > systemDirPath.size()) {
        systemDirPath.resize(systemDirLength + 1, '\0');
        systemDirLength = GetSystemDirectoryW(&systemDirPath[0], systemDirPath.size());

        if (!systemDirLength)
            return false;
    }

    systemDirPath.resize(systemDirLength);

    std::wostringstream commandLineBuffer;
    commandLineBuffer << systemDirPath << L"\\vsjitdebugger.exe -p " << pid;
    std::wstring commandLine = commandLineBuffer.str();

    // Start Visual Studio JIT Debugger
    STARTUPINFOW si = { 0 };
    si.cb = sizeof(si);

    PROCESS_INFORMATION pi = { 0 };
    ZeroMemory(&pi, sizeof(pi));

    BOOL processCreated = CreateProcessW(
        /* lpApplicationName     */ NULL, 
        /* lpCommandLine         */ &commandLine[0], 
        /* lpProcessAttributes   */ NULL,
        /* lpThreadAttributes    */ NULL, 
        /* bInheritHandles       */ FALSE, 
        /* dwCreationFlags       */ NULL,
        /* lpEnvironment         */ NULL, 
        /* lpCurrentDirectory    */ NULL, 
        /* lpStartupInfo         */ &si, 
        /* lpProcessInformation  */ &pi
    );

    if (!processCreated)
        return false;

    WaitForSingleObject(pi.hProcess, INFINITE);
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);
    return true;
}
