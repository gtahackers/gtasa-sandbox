#include "RemoteModule.h"
#include "GetRemoteModuleHandle.h"
#include "GetRemoteProcAddress.h"
#include <Windows.h>

RemoteModule::RemoteModule(const char* moduleName)
    : m_hProcess(NULL)
    , m_hModule(GetModuleHandleA(moduleName))
{
}

RemoteModule::RemoteModule(const wchar_t* moduleName)
    : m_hProcess(NULL)
    , m_hModule(GetModuleHandleW(moduleName))
{
}

RemoteModule::RemoteModule(const std::string& moduleName)
    : RemoteModule(moduleName.c_str())
{
}

RemoteModule::RemoteModule(const std::wstring& moduleName)
    : RemoteModule(moduleName.c_str())
{
}

RemoteModule::RemoteModule(HANDLE hProcess, const char* moduleName)
    : m_hProcess(hProcess)
    , m_hModule(GetRemoteModuleHandleA(hProcess, moduleName))
{
}

RemoteModule::RemoteModule(HANDLE hProcess, const wchar_t* moduleName)
    : m_hProcess(hProcess)
    , m_hModule(GetRemoteModuleHandleW(hProcess, moduleName))
{
}

RemoteModule::RemoteModule(HANDLE hProcess, const std::string& moduleName)
    : RemoteModule(hProcess, moduleName.c_str())
{
}

RemoteModule::RemoteModule(HANDLE hProcess, const std::wstring& moduleName)
    : RemoteModule(hProcess, moduleName.c_str())
{
}

RemoteModule::RemoteModule(HMODULE hModule)
    : m_hProcess(NULL)
    , m_hModule(hModule)
{
}

RemoteModule::RemoteModule(HANDLE hProcess, HMODULE hModule)
    : m_hProcess(hProcess)
    , m_hModule(hModule)
{
}

DWORD RemoteModule::CallInternal(const char* functionName, const void* argument, const std::size_t size)
{
    if (!m_hProcess || !m_hModule)
        return NULL;

    FARPROC pFunction = GetRemoteProcAddress(m_hProcess, m_hModule, functionName);

    if (!pFunction)
        return NULL;

    return CallRemoteFunction(m_hProcess, pFunction, argument, size);
}

DWORD RemoteModule::CallSharedInternal(HANDLE hProcess, const char* functionName, const void* argument, const std::size_t size)
{
    if (!m_hModule)
        return NULL;

    FARPROC pFunction = GetProcAddress(m_hModule, functionName);

    if (!pFunction)
        return NULL;

    return CallRemoteFunction(hProcess, pFunction, argument, size);
}

DWORD RemoteModule::CallRemoteFunction(HANDLE hProcess, FARPROC pFunction, const void* argument, const std::size_t size)
{
    LPVOID pRemoteBaseAddress = NULL;

    if (size > 0) {
        pRemoteBaseAddress = VirtualAllocEx(
            /* hProcess         */ hProcess, 
            /* lpAddress        */ NULL, 
            /* dwSize           */ size, 
            /* flAllocationType */ MEM_RESERVE | MEM_COMMIT, 
            /* flProtect        */ PAGE_EXECUTE_READWRITE
        );

        if (!pRemoteBaseAddress)
            return NULL;

        SIZE_T bytesWritten = NULL;

        BOOL bPathWritten = WriteProcessMemory(
            /* hProcess               */ hProcess, 
            /* lpBaseAddress          */ pRemoteBaseAddress, 
            /* lpBuffer               */ argument, 
            /* nSize                  */ size, 
            /* lpNumberOfBytesWritten */ &bytesWritten
        );

        if (!bPathWritten || bytesWritten != size) {
            VirtualFreeEx(
                /* hProcess   */ hProcess, 
                /* lpAddress  */ pRemoteBaseAddress, 
                /* dwSize     */ size, 
                /* dwFreeType */ MEM_RELEASE
            );
            
            return NULL;
        }
    }

    HANDLE hThread = CreateRemoteThread(
        /* hProcess           */ hProcess, 
        /* lpThreadAttributes */ NULL, 
        /* dwStackSize        */ NULL, 
        /* lpStartAddress     */ reinterpret_cast<LPTHREAD_START_ROUTINE>(pFunction), 
        /* lpParameter        */ pRemoteBaseAddress, 
        /* dwCreationFlags    */ NULL, 
        /* lpThreadId         */ NULL
    );

    if (hThread == NULL) {
        if (pRemoteBaseAddress)
            VirtualFreeEx(hProcess, pRemoteBaseAddress, size, MEM_RELEASE);
        
        return NULL;
    }

    SetThreadPriority(
        /* hThread   */ hThread, 
        /* nPriority */ THREAD_PRIORITY_TIME_CRITICAL
    );

    WaitForSingleObject(
        /* hHandle        */ hThread, 
        /* dwMilliseconds */ INFINITE
    );

    DWORD hExitCode = NULL;

    GetExitCodeThread(
        /* hThread    */ hThread, 
        /* lpExitCode */ &hExitCode
    );

    CloseHandle(hThread);

    if (pRemoteBaseAddress)
        VirtualFreeEx(hProcess, pRemoteBaseAddress, size, MEM_RELEASE);
    
    return hExitCode;
}
