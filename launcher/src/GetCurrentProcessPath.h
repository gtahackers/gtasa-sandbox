#pragma once
#include <filesystem>

namespace fs = std::experimental::filesystem;

fs::path GetCurrentProcessPath();
