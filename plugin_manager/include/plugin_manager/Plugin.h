#pragma once

#define PLUGIN_DECL extern "C" __declspec(dllexport)
#define PLUGIN_API  __cdecl

PLUGIN_DECL void PLUGIN_API OnPluginStart(void);
PLUGIN_DECL void PLUGIN_API OnPluginStop(void);
