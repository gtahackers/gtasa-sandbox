#pragma once

#include <minwindef.h>

#ifdef BUILD_PLUGIN_MANAGER_DLL
#   define PLUGIN_MANAGER_DECL extern "C" __declspec(dllexport)
#else
#   define PLUGIN_MANAGER_DECL extern "C" __declspec(dllimport)
#endif

namespace PluginManager
{
    int const MAX_DIRECTORY_LEN = 256;

    struct InitData
    {
        char szDirectory[MAX_DIRECTORY_LEN];
    };
}

// See THREAD_START_ROUTINE
PLUGIN_MANAGER_DECL DWORD WINAPI OnPluginManagerStart(LPVOID lpParameter);
