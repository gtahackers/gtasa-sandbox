#include "AppEventHandler.h"
#include "Globals.h"
#include <iostream>
#include <Windows.h>
#include <detours.h>
#include <chrono>

AppEventHandler::AppEventHandler()
{
    CreateFunctionHook();
}

AppEventHandler::~AppEventHandler()
{
    DestroyFunctionHook();
}

bool AppEventHandler::CreateFunctionHook()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&AppEventHandler::m_GameFunction);
    auto pDetour   = reinterpret_cast<PVOID>(&AppEventHandler::FunctionHook);
    DetourAttach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}

bool AppEventHandler::DestroyFunctionHook()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    auto ppPointer = reinterpret_cast<PVOID *>(&AppEventHandler::m_GameFunction);
    auto pDetour   = reinterpret_cast<PVOID>(&AppEventHandler::FunctionHook);
    DetourDetach(ppPointer, pDetour);
    return DetourTransactionCommit() == NO_ERROR;
}

RsEventStatus __cdecl AppEventHandler::FunctionHook(RsEvent event, void* param)
{
    using clock = std::chrono::high_resolution_clock;
    static clock::time_point lastUpdateTime;

    if (event == rsIDLE) {
        clock::time_point const now = clock::now();

        if ((now - lastUpdateTime) >= std::chrono::seconds(5)) {
            if (g_pLibraryWatchdog)
                g_pLibraryWatchdog->Update();
            
            lastUpdateTime = now;
        }
    }

    return m_GameFunction(event, param);
}
