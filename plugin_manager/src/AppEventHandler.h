#pragma once

enum RsEventStatus
{
    rsEVENTERROR,
    rsEVENTPROCESSED,
    rsEVENTNOTPROCESSED
};

enum RsEvent
{
    rsCAMERASIZE,
    rsCOMMANDLINE,
    rsFILELOAD,
    rsINITDEBUG,
    rsINPUTDEVICEATTACH,
    rsLEFTBUTTONDOWN,
    rsLEFTBUTTONUP,
    rsMOUSEMOVE,
    rsMOUSEWHEELMOVE,
    rsPLUGINATTACH,
    rsREGISTERIMAGELOADER,
    rsRIGHTBUTTONDOWN,
    rsRIGHTBUTTONUP,
    rsRWINITIALIZE = 21,
    rsRWTERMINATE,
    rsSELECTDEVICE,
    rsINITIALIZE,
    rsTERMINATE,
    rsIDLE,
    rsKEYDOWN,
    rsKEYUP,
    rsQUITAPP,
    rsPADBUTTONDOWN,
    rsPADBUTTONUP,
    rsPADANALOGUELEFT,
    rsPADANALOGUELEFTRESET,
    rsPADANALOGUERIGHT,
    rsPADANALOGUERIGHTRESET,
    rsPREINITCOMMANDLINE,
    rsACTIVATE,
    rsSETMEMORYFUNCS
};

class AppEventHandler
{
    using Function_t = RsEventStatus (__cdecl *)(RsEvent event, void *param);

public:
    AppEventHandler();
    AppEventHandler(AppEventHandler const&) = delete;
    AppEventHandler(AppEventHandler &&) = default;
    AppEventHandler& operator=(AppEventHandler const&) = delete;
    AppEventHandler& operator=(AppEventHandler &&) = default;
    ~AppEventHandler();

private:
    bool CreateFunctionHook();
    bool DestroyFunctionHook();

    static RsEventStatus __cdecl FunctionHook(RsEvent event, void* param);
    inline static Function_t m_GameFunction = reinterpret_cast<Function_t>(0x53EC10);
};
