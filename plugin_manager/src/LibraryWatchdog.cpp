#include "LibraryWatchdog.h"
#include <string>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <Windows.h>

inline bool ends_with(std::string const& value, std::string const& ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

LibraryWatchdog::LibraryWatchdog(fs::path const& libraryDirectory)
    : m_LibraryDirectory(libraryDirectory)
    , m_StaticPlugins()
    , m_HotPlugins()
{
    GetTempDirectory(m_TempDirectory);
    LoadLibraries();
}

LibraryWatchdog::~LibraryWatchdog()
{
    for (HMODULE& hModule : m_StaticPlugins)
    {
        CallPluginStop(hModule);
        FreeLibrary(hModule);
    }

    for (Plugin& plugin : m_HotPlugins)
    {
        CallPluginStop(plugin.hModule);
        FreeLibrary(plugin.hModule);
        CloseHandle(plugin.hFile);
    }
}

void LibraryWatchdog::Update()
{
    std::string const tempDirPath = m_TempDirectory.string();

    using PluginVector = std::vector<Plugin>;

    for (PluginVector::iterator it = m_HotPlugins.begin(); it != m_HotPlugins.end(); /* manual increment */)
    {
        Plugin& plugin = *it;

        WIN32_FILE_ATTRIBUTE_DATA fileAttributes = { 0 };
        
        if (!GetFileAttributesExA(plugin.sourceFile.c_str(), GetFileExInfoStandard, &fileAttributes)) {
            ++it;
            continue;
        }
        
        if (CompareFileTime(&fileAttributes.ftLastWriteTime, &plugin.lastWriteTime) < 1) {
            ++it;
            continue;
        }
        
        CallPluginStop(plugin.hModule);
        FreeLibrary(plugin.hModule);
        CloseHandle(plugin.hFile);

        if (!CreateTempCopy(plugin.sourceFile, tempDirPath, plugin.hFile, plugin.activeFile)) {
            std::cout << "Error during hot-reload for " << plugin.pluginName << ": Temporary copy failed\n";
            it = m_HotPlugins.erase(it);
            continue;
        }

        plugin.hModule = LoadLibraryA(plugin.activeFile.c_str());

        if (!plugin.hModule) {
            std::cout << "Error during hot-reload for " << plugin.pluginName << ": LoadLibrary returned NULL\n";
            CloseHandle(plugin.hFile);
            it = m_HotPlugins.erase(it);
            continue;
        }

        CallPluginStart(plugin.hModule);
        plugin.lastWriteTime = fileAttributes.ftLastWriteTime;

        std::cout << "Plugin " << plugin.pluginName << " reloaded\n";
        ++it;
    }
}

void LibraryWatchdog::LoadLibraries()
{
    std::cout << "Library directory:   " << m_LibraryDirectory.string() << "\n";
    std::cout << "Temporary directory: " << m_TempDirectory.string() << "\n";

    std::vector<std::string> pluginList;

    for (fs::path const& file : fs::directory_iterator(m_LibraryDirectory))
    {
        if (!fs::is_regular_file(file))
            continue;

        std::string const fileName = file.filename().string();

        if (!ends_with(fileName, "_plugin.dll"))
            continue;

        pluginList.emplace_back(file.string());
    }

    if (pluginList.empty()) {
        std::cout << "Note: No plugins found\n";
        return;
    }

    std::cout << "Found " << pluginList.size() << " plugins\n";
    std::string const tempDirPath = m_TempDirectory.string();

    for (std::string const& pluginPath : pluginList)
    {
        HANDLE            hFile = INVALID_HANDLE_VALUE;
        std::string       tempFilePath;
        std::string const pluginName = fs::path(pluginPath).filename().string();

        if (CreateTempCopy(pluginPath, tempDirPath, hFile, tempFilePath)) {
            FILETIME lastWriteTime = { 0 };

            if (!GetFileTime(hFile, NULL, NULL, &lastWriteTime)) {
                std::cout << "Loading " << pluginName << "failed: Could not fetch last file write time\n";
                CloseHandle(hFile);
                continue;
            }

            HMODULE hModule = LoadLibraryA(tempFilePath.c_str());

            if (!hModule) {
                std::cout << "Loading " << pluginName << "failed: LoadLibrary returned NULL\n";
                CloseHandle(hFile);
                continue;
            }
            
            CallPluginStart(hModule);

            m_HotPlugins.push_back({
                /* hModule       */ hModule,
                /* hFile         */ hFile,
                /* lastWriteTime */ lastWriteTime,
                /* pluginName    */ pluginName,
                /* sourceFile    */ pluginPath,
                /* activeFile    */ std::move(tempFilePath)
            });
        }
        else {
            HMODULE hModule = LoadLibraryA(pluginPath.c_str());

            if (!hModule) {
                std::cout << "Loading " << pluginName << "failed: LoadLibrary returned NULL\n";
                continue;
            }

            CallPluginStart(hModule);
            m_StaticPlugins.push_back(hModule);
        }

        std::cout << "Plugin " << pluginName << " started\n";
    }
}

void LibraryWatchdog::CallPluginStart(HMODULE const hModule) const
{
    using OnPluginStart_t = void (__cdecl *)(void);
    auto const OnPluginStart = reinterpret_cast<OnPluginStart_t>(GetProcAddress(hModule, "OnPluginStart"));

    if (OnPluginStart != nullptr)
        OnPluginStart();
}

void LibraryWatchdog::CallPluginStop(HMODULE const hModule) const
{
    using OnPluginStop_t = void (__cdecl *)(void);
    auto const OnPluginStop = reinterpret_cast<OnPluginStop_t>(GetProcAddress(hModule, "OnPluginStop"));

    if (OnPluginStop != nullptr)
        OnPluginStop();
}

bool LibraryWatchdog::GetTempDirectory(fs::path& outDirectory)
{
    outDirectory.clear();

    char tempPath[MAX_PATH];
    DWORD const length = GetTempPathA(MAX_PATH, tempPath);

    if (!length || length > MAX_PATH)
        return false;
    
    char tempFilePath[MAX_PATH];

    if (!GetTempFileNameA(tempPath, "LWD", 0, tempFilePath))
        return false;

    DeleteFileA(tempFilePath);
    strcat_s(tempFilePath, ".GTASA-SANDBOX");

    if (!CreateDirectoryA(tempFilePath, NULL))
        return false;
    
    outDirectory = fs::path(tempFilePath);
    return true;
}

bool LibraryWatchdog::CreateTempCopy(std::string const& filePath, std::string const& tempPath, HANDLE& hFile, std::string& path)
{
    path.clear();
    hFile = INVALID_HANDLE_VALUE;

    if (filePath.empty() || tempPath.empty())
        return false;

    char tempFilePath[MAX_PATH];

    if (!GetTempFileNameA(tempPath.c_str(), "LWD", 0, tempFilePath))
        return false;

    if (!CopyFileA(filePath.c_str(), tempFilePath, FALSE))
        return false;

    hFile = CreateFileA(
        /* lpFileName            */ tempFilePath, 
        /* dwDesiredAccess       */ GENERIC_READ, 
        /* dwShareMode           */ FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, 
        /* lpSecurityAttributes  */ NULL,
        /* dwCreationDisposition */ OPEN_EXISTING,
        /* dwFlagsAndAttributes  */ FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE,
        /* hTemplateFile         */ NULL
    );

    if (hFile != INVALID_HANDLE_VALUE)
        path = std::string(tempFilePath);

    return true;
}
