#include "PluginManager.h"
#include "Globals.h"
#include "LibraryWatchdog.h"
#include "AppEventHandler.h"
#include <cstdio>
#include <iostream>
#include <Windows.h>

void CreateTerminal();
bool CreateAppEventHandler();
bool CreateLibraryWatchdog(fs::path const& libraryDirectory);

static bool                             pluginManagerInitialized = false;
static std::unique_ptr<AppEventHandler> pAppEventHandler         = nullptr;

std::unique_ptr<LibraryWatchdog> g_pLibraryWatchdog = nullptr;

PLUGIN_MANAGER_DECL DWORD WINAPI OnPluginManagerStart(LPVOID lpParameter)
{
    if (pluginManagerInitialized)
        return FALSE;
    else
        pluginManagerInitialized = true;
    
    CreateTerminal();

    if (!lpParameter) {
        std::cerr << "OnPluginManagerStart: Error: Parameter is NULL\n";
        return FALSE;
    }

    if (!CreateAppEventHandler())
        return FALSE;

    auto pInitData = reinterpret_cast<PluginManager::InitData *>(lpParameter);

    if (!CreateLibraryWatchdog(fs::path(pInitData->szDirectory))) {
        pAppEventHandler.reset();
        return FALSE;
    }

    return TRUE;
}

void CreateTerminal()
{
    AllocConsole();
    SetConsoleTitle(TEXT("GTA:SA Console"));
    std::freopen("CONOUT$", "wt", stdout);
    std::freopen("CONOUT$", "wt", stderr);
}

bool CreateAppEventHandler()
{
    try {
        pAppEventHandler = std::make_unique<AppEventHandler>();
        return true;
    }
    catch (std::bad_alloc& exception) {
        std::cerr << "OnPluginManagerStart: Error: Failed to allocate memory for AppEventHandler: " << exception.what() << "\n";
        return false;
    }
}

bool CreateLibraryWatchdog(fs::path const& libraryDirectory)
{
    try {
        g_pLibraryWatchdog = std::make_unique<LibraryWatchdog>(libraryDirectory);
        return true;
    }
    catch (std::bad_alloc& exception) {
        std::cerr << "OnPluginManagerStart: Error: Failed to allocate memory for LibraryWatchdog: " << exception.what() << "\n";
        return false;
    }
}
