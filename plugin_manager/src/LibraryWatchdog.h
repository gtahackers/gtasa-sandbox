#pragma once

#include <filesystem>
#include <vector>
#include <minwindef.h>
#include <minwinbase.h>

namespace fs = std::experimental::filesystem;

class LibraryWatchdog
{
public:
    LibraryWatchdog(fs::path const& libraryDirectory);
    LibraryWatchdog(LibraryWatchdog const&) = delete;
    LibraryWatchdog(LibraryWatchdog &&) = default;
    LibraryWatchdog& operator=(LibraryWatchdog const&) = delete;
    LibraryWatchdog& operator=(LibraryWatchdog &&) = default;
    ~LibraryWatchdog();

    void Update();

private:
    void LoadLibraries();
    void CallPluginStart(HMODULE const hModule) const;
    void CallPluginStop(HMODULE const hModule) const;

    static bool GetTempDirectory(fs::path& outDirectory);

    static bool CreateTempCopy(std::string const& filePath, 
                               std::string const& tempPath, 
                               HANDLE& hFile, 
                               std::string& path);

private:
    struct Plugin
    {
        HMODULE     hModule;
        HANDLE      hFile;
        FILETIME    lastWriteTime;
        std::string pluginName;
        std::string sourceFile;
        std::string activeFile;
    };

    fs::path             m_LibraryDirectory;
    fs::path             m_TempDirectory;
    std::vector<HMODULE> m_StaticPlugins;
    std::vector<Plugin>  m_HotPlugins;
};
